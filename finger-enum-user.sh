#!/bin/bash

# http://stackoverflow.com/questions/10929453/read-a-file-line-by-line-assigning-the-value-to-a-variable
display_usage()
{
	echo -e "\nScript takes a file with a list of users as argument"
	echo -e "Usage:\n./finger-enum-user.sh <filename.txt> <ip>\n"
}

if [ $# -le 1 ]
then
	display_usage
	exit 1
fi

while IFS='' read -r line || [[ -n "$line" ]]; do

	echo "User :" $line
	finger $line@$2
	echo -e "\n"

done < "$1"
